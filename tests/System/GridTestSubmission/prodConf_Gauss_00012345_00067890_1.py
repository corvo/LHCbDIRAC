from ProdConf import ProdConf

ProdConf(
  NOfEvents=2,
  DDDBTag='dddb-20130929',
  AppVersion='v45r5',
  XMLSummaryFile='summaryGauss_00012345_00067890_1.xml',
  Application='Gauss',
  OutputFilePrefix='00012345_00067890_1',
  RunNumber=2308595,
  XMLFileCatalog='pool_xml_catalog.xml',
  FirstEventNumber=518801,
  CondDBTag='sim-20130522-vc-md100',
  OutputFileTypes=['sim'],
)
