from ProdConf import ProdConf

ProdConf(
  NOfEvents=2,
  DDDBTag='Sim08-20130503-1',
  AppVersion='v45r3',
  XMLSummaryFile='summaryGauss_00012345_00067890_1.xml',
  Application='Gauss',
  OutputFilePrefix='00012345_00067890_1',
  RunNumber=2308595,
  XMLFileCatalog='pool_xml_catalog.xml',
  FirstEventNumber=518801,
  CondDBTag='Sim08-20130503-1-vc-mu100',
  OutputFileTypes=['sim'],
)
